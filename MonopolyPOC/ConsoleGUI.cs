﻿using MonopolyPOC.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonopolyPOC
{
    class ConsoleGUI
    {
        internal static void ShowLabyrinth(Labyrinth labyrinth)
        {
           for(int i = 0; i < labyrinth.Rows; i++)
            {
                string t = "";
                for(int j = 0; j < labyrinth.Cols; j++)
                {
                    t += labyrinth.Matrix[i, j].ToString();
                }
                Console.WriteLine(t);
            }
        }
    }
}
