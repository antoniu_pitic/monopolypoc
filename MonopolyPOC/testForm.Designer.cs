﻿namespace MonopolyPOC
{
    partial class testForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.saveButton = new System.Windows.Forms.Button();
            this.loadButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.loadJsonButton = new System.Windows.Forms.Button();
            this.saveJsonButton = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.loadMatrixButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.matrixGroupBox = new System.Windows.Forms.GroupBox();
            this.monopolyGroupBox = new System.Windows.Forms.GroupBox();
            this.startMonopolyButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.matrixGroupBox.SuspendLayout();
            this.monopolyGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(34, 48);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(133, 53);
            this.saveButton.TabIndex = 0;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // loadButton
            // 
            this.loadButton.Location = new System.Drawing.Point(227, 48);
            this.loadButton.Name = "loadButton";
            this.loadButton.Size = new System.Drawing.Size(124, 53);
            this.loadButton.TabIndex = 1;
            this.loadButton.Text = "Load";
            this.loadButton.UseVisualStyleBackColor = true;
            this.loadButton.Click += new System.EventHandler(this.loadButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.saveButton);
            this.groupBox1.Controls.Add(this.loadButton);
            this.groupBox1.Location = new System.Drawing.Point(64, 26);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(430, 162);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Serilization";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.loadJsonButton);
            this.groupBox2.Controls.Add(this.saveJsonButton);
            this.groupBox2.Location = new System.Drawing.Point(64, 240);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(430, 162);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Json";
            // 
            // loadJsonButton
            // 
            this.loadJsonButton.Location = new System.Drawing.Point(227, 62);
            this.loadJsonButton.Name = "loadJsonButton";
            this.loadJsonButton.Size = new System.Drawing.Size(142, 51);
            this.loadJsonButton.TabIndex = 1;
            this.loadJsonButton.Text = "Load";
            this.loadJsonButton.UseVisualStyleBackColor = true;
            this.loadJsonButton.Click += new System.EventHandler(this.loadJsonButton_Click);
            // 
            // saveJsonButton
            // 
            this.saveJsonButton.Location = new System.Drawing.Point(45, 62);
            this.saveJsonButton.Name = "saveJsonButton";
            this.saveJsonButton.Size = new System.Drawing.Size(128, 51);
            this.saveJsonButton.TabIndex = 0;
            this.saveJsonButton.Text = "Save";
            this.saveJsonButton.UseVisualStyleBackColor = true;
            this.saveJsonButton.Click += new System.EventHandler(this.saveJsonButton_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(64, 422);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 82;
            this.dataGridView1.RowTemplate.Height = 33;
            this.dataGridView1.Size = new System.Drawing.Size(430, 361);
            this.dataGridView1.TabIndex = 4;
            // 
            // loadMatrixButton
            // 
            this.loadMatrixButton.Location = new System.Drawing.Point(48, 48);
            this.loadMatrixButton.Name = "loadMatrixButton";
            this.loadMatrixButton.Size = new System.Drawing.Size(179, 53);
            this.loadMatrixButton.TabIndex = 0;
            this.loadMatrixButton.Text = "Load Matrix";
            this.loadMatrixButton.UseVisualStyleBackColor = true;
            this.loadMatrixButton.Click += new System.EventHandler(this.loadMatrixButton_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.panel1.Location = new System.Drawing.Point(87, 671);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(60, 57);
            this.panel1.TabIndex = 2;
            this.panel1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseClick);
            // 
            // matrixGroupBox
            // 
            this.matrixGroupBox.Controls.Add(this.panel1);
            this.matrixGroupBox.Controls.Add(this.loadMatrixButton);
            this.matrixGroupBox.Location = new System.Drawing.Point(534, 26);
            this.matrixGroupBox.Name = "matrixGroupBox";
            this.matrixGroupBox.Size = new System.Drawing.Size(377, 757);
            this.matrixGroupBox.TabIndex = 5;
            this.matrixGroupBox.TabStop = false;
            this.matrixGroupBox.Text = "Matrix";
            // 
            // monopolyGroupBox
            // 
            this.monopolyGroupBox.Controls.Add(this.startMonopolyButton);
            this.monopolyGroupBox.Location = new System.Drawing.Point(942, 27);
            this.monopolyGroupBox.Name = "monopolyGroupBox";
            this.monopolyGroupBox.Size = new System.Drawing.Size(624, 756);
            this.monopolyGroupBox.TabIndex = 6;
            this.monopolyGroupBox.TabStop = false;
            this.monopolyGroupBox.Text = "Monopoly";
            // 
            // startMonopolyButton
            // 
            this.startMonopolyButton.Location = new System.Drawing.Point(211, 47);
            this.startMonopolyButton.Name = "startMonopolyButton";
            this.startMonopolyButton.Size = new System.Drawing.Size(175, 53);
            this.startMonopolyButton.TabIndex = 0;
            this.startMonopolyButton.Text = "Start Monopoly";
            this.startMonopolyButton.UseVisualStyleBackColor = true;
            this.startMonopolyButton.Click += new System.EventHandler(this.startMonopolyButton_Click);
            // 
            // testForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1855, 795);
            this.Controls.Add(this.monopolyGroupBox);
            this.Controls.Add(this.matrixGroupBox);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "testForm";
            this.Text = "tests";
            this.Load += new System.EventHandler(this.testForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.matrixGroupBox.ResumeLayout(false);
            this.monopolyGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button loadButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button saveJsonButton;
        private System.Windows.Forms.Button loadJsonButton;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button loadMatrixButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox matrixGroupBox;
        private System.Windows.Forms.GroupBox monopolyGroupBox;
        private System.Windows.Forms.Button startMonopolyButton;
    }
}

