﻿using MonopolyPOC.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonopolyPOC
{
    class PlayerDataProvider : IDataProvider
    {
        internal static List<Player> GetAll()
        {
            string serializedPlayers = File.ReadAllText("player.json");
            return JsonConvert.DeserializeObject<List<Player>>(serializedPlayers);
        }
    }
}
