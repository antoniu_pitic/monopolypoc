﻿using MonopolyPOC.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonopolyPOC
{
    class MatrixDataProvider
    {
        internal static Labyrinth Get()
        {
            Labyrinth t = new Labyrinth();

            string allMatrix = File.ReadAllText("Matrice.txt");
            var allLines = allMatrix.Split('\n');
            
            var dimensions = allLines[0].Split(' ');
            t.Rows = Convert.ToInt32(dimensions[0]);
            t.Cols = Convert.ToInt32(dimensions[1]);
            
            for (int i = 0; i < t.Rows; i++)
            {
                var lineValues = allLines[i + 1].Split(' ');
                for (int j = 0; j < t.Cols; j++)
                {
                    t.Matrix[i, j] = Convert.ToInt32(lineValues[j]);
                }
            }

            return t;
        }
    }
}
