﻿using MonopolyPOC.Entities;

namespace MonopolyPOC
{
    internal class Cell
    {
        int x, y;
        int height, width;
        string name;
        string picturePath; 
        int purchaseSum;
        int rentalSum;
        Player Owner;
    }
}