﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonopolyPOC.Entities
{
    class Labyrinth
    {
        int rows;
        int cols;
        int[,] matrix = new int[100, 100];

        public int Rows { get => rows; set => rows = value; }
       
        public int[,] Matrix { get => matrix; set => matrix = value; }
        public int Cols { get => cols; set => cols = value; }
    }
}
