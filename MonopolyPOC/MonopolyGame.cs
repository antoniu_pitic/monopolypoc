﻿using MonopolyPOC.Entities;
using System;
using System.Collections.Generic;

namespace MonopolyPOC
{
    internal class MonopolyGame
    {
        Dice dice = new Dice();
        List<Player> players = new List<Player>();
        Board board = new Board();

        public MonopolyGame()
        {
        }

        internal void Start()
        {
            Console.WriteLine("Monopoly Game FTW");
            Init();
            Run();
        }

        private void Run()
        {
            while (!Winner())
            {
                foreach (var player in players)
                {
                    player.Play();
                }
            }
        }

        private bool Winner()
        {
            throw new NotImplementedException();
        }

        private void Init()
        {
            throw new NotImplementedException();
        }
    }
}