﻿using MonopolyPOC.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MonopolyPOC
{
    public partial class testForm : Form
    {
        Panel[,] viewLabyrinth = new Panel[100, 100];
        public testForm()
        {
            InitializeComponent();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            Player p = new Player();
            p.Name = "Toni";
            p.Money = 213;

            IFormatter formatter = new BinaryFormatter();
            Stream t = new FileStream(@"test.txt", FileMode.Create, FileAccess.Write);
            formatter.Serialize(t, p);
            t.Close();

        }

        private void loadButton_Click(object sender, EventArgs e)
        {
            Stream t = new FileStream(@"test.txt", FileMode.Open, FileAccess.Read);
            IFormatter formatter = new BinaryFormatter();
            Player x = (Player)formatter.Deserialize(t);

            Console.WriteLine(x.Name + " " + x.Money);
        }

        private void saveJsonButton_Click(object sender, EventArgs e)
        {
            List<Player> playerList = new List<Player>();
            MockPlayers(playerList);

            string serializedPlayer = JsonConvert.SerializeObject(playerList);
            File.WriteAllText("player.json", serializedPlayer);
        }

        private static void MockPlayers(List<Player> playerList)
        {
            playerList.Add(new Player { Name = "Mario", Money = 556 });
            playerList.Add(new Player { Name = "Luigi", Money = -10 });
            playerList.Add(new Player { Name = "Toni", Money = 123 });
            playerList.Add(new Player { Name = "Sandu", Money = 32 });
        }

        private void loadJsonButton_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = PlayerDataProvider.GetAll();
        }

        private void testForm_Load(object sender, EventArgs e)
        {

        }

        private void loadMatrixButton_Click(object sender, EventArgs e)
        {
            Labyrinth labyrinth = new Labyrinth();
            labyrinth = MatrixDataProvider.Get();
            ConsoleGUI.ShowLabyrinth(labyrinth);
            ShowLabyrinth(labyrinth);
        }

        private void ShowLabyrinth(Labyrinth labyrinth)
        {
            for (int i = 0; i < labyrinth.Rows; i++)
            {
                for (int j = 0; j < labyrinth.Cols; j++)
                {
                    viewLabyrinth[i, j] = new Panel();
                    viewLabyrinth[i, j].BackColor = GetColorsByValue(labyrinth.Matrix[i, j]);
                    viewLabyrinth[i, j].Height = 20;
                    viewLabyrinth[i, j].Width = 20;
                    viewLabyrinth[i, j].Top = 100 + i * 22;
                    viewLabyrinth[i, j].Left = 10 + j * 22;

                    viewLabyrinth[i,j].MouseClick += new System.Windows.Forms.MouseEventHandler(ClickOnPanel);

                    matrixGroupBox.Controls.Add(viewLabyrinth[i, j]);
                }

            }
        }

        private void ClickOnPanel(object sender, MouseEventArgs e)
        {
            ((Panel)sender).BackColor = Color.Red;
        }

        private Color GetColorsByValue(int value)
        {
            switch (value)
            {
                case 0: return Color.Yellow;
                case 1: return Color.Blue;
                default:
                    return Color.Lime;

            }
        }

        private void panel1_MouseClick(object sender, MouseEventArgs e)
        {
            ((Panel)sender).BackColor = Color.Red;
        }

        private void startMonopolyButton_Click(object sender, EventArgs e)
        {
            MonopolyGame game = new MonopolyGame();
            game.Start();
        }
    }
}